package com.org.hcl.greatLearning.assignment;

public class AdminDepartment extends SuperDepartment {

			//declare method departmentName of return type string
			public String depName() {
			return "ADMIN DEPARTMENT";
			}

			//declare method getTodaysWork of return type string
			public String getTodaysWork() {
			return "COMPLETE YOUR DOCUMENTS SUBMISSION";
			}

			//declare method getWorkDeadline of return type string
			public String getWorkDeadline() {
			return "COMPLETE BY END OF THE DAY ";
	}
			public static void main(String[] args) {
				AdminDepartment pri1 = new AdminDepartment();
				  System.out.println(pri1.depName());
				  System.out.println(pri1.getTodaysWork());
				  System.out.println(pri1.getWorkDeadline());
			}
}
